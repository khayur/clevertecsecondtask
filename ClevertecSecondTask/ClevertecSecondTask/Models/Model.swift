//
//  Model.swift
//  ClevertecSecondTask
//
//  Created by Yury Khadatovich on 13.02.22.
//

import Foundation

class Model {
    
    //MARK: - Static Properties
    
    static let shared = Model()
    
    //MARK: -Public Properties
    
    var rowsIndexes = Array(1...1000)
    
    //MARK: -Initializers
    
    private init() {}
}
