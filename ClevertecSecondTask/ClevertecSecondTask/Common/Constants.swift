//
//  Constants.swift
//  ClevertecSecondTask
//
//  Created by Yury Khadatovich on 13.02.22.
//

import UIKit

struct Constants {
    static let cellTitleLabelText = "Title "
    static let cellDescriptionLabelText = "Description "
    static let cellTitleLabelFont: UIFont = .systemFont(ofSize: 18)
    static let cellDescriptionLabelFont: UIFont = .systemFont(ofSize: 17)
    static let defaultPadding: CGFloat = 15
    static let defaultRowHeight: CGFloat = 80
    static let cellIconImageScaling = 1.7
    static let animationDuration = 0.25
    static let iconBackgroungColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
    static let defaultBackgroundColor: UIColor = .white
    static let cellBackgroundColor = UIColor(red: 212/255, green: 241/255, blue: 254/255, alpha: 1)
}
