//
//  BaseViewController.swift
//  ClevertecSecondTask
//
//  Created by Yury Khadatovich on 13.02.22.
//

import UIKit

class BaseViewController: UIViewController {
    
    // MARK: - Public Properties
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    //MARK: -Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.defaultBackgroundColor
    }
    
}
