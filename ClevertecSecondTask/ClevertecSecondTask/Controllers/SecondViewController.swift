//
//  SecondViewController.swift
//  ClevertecSecondTask
//
//  Created by Yury Khadatovich on 10.02.22.
//

import UIKit

class SecondViewController: BaseViewController {
    
    // MARK: - Public Properties
    
    var cellData: (iconImage: UIImage?, titleLabelText: String?, descriptionLabelText: String?)?
    var imageSize: CGSize?
    
    // MARK: - Private Properties
    
    private lazy var iconView: UIView = {
        let iconSide = (imageSize?.height ?? 0) * 2
        let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: iconSide, height: iconSide)))
        view.layer.cornerRadius = view.frame.height / 2
        view.backgroundColor = Constants.iconBackgroungColor
        view.addSubview(iconImageView)
        return view
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = cellData?.iconImage?.withTintColor(.black)
        return imageView
    }()
    
    private lazy var labelsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Constants.defaultPadding / 4
        stack.alignment = .center
        stack.distribution = .fillEqually
        [titleLabel, descriptionLabel].forEach { stack.addArrangedSubview($0) }
        return stack
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = cellData?.titleLabelText
        label.font = Constants.cellTitleLabelFont
        label.textColor = .darkGray
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = cellData?.descriptionLabelText
        label.font = Constants.cellDescriptionLabelFont
        label.textColor = .lightGray
        return label
    }()
    
    private var iconSide: CGFloat {
        return (imageSize?.height ?? 0) * 2
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViewController()
        activateConstraints()
    }
    
    // MARK: - Private Methods
    private func configureViewController() {
        configureNavigationBar()
        
        self.view.addSubview(iconView)
        self.view.addSubview(labelsStackView)
    }
    
    private func configureNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    private func activateConstraints() {
        
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        labelsStackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            iconView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            iconView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            iconView.widthAnchor.constraint(equalToConstant: iconSide),
            iconView.heightAnchor.constraint(equalToConstant: iconSide),
            iconImageView.centerXAnchor.constraint(equalTo: iconView.centerXAnchor),
            iconImageView.centerYAnchor.constraint(equalTo: iconView.centerYAnchor),
            iconImageView.widthAnchor.constraint(equalToConstant: iconSide / Constants.cellIconImageScaling),
            iconImageView.heightAnchor.constraint(equalToConstant: iconSide / Constants.cellIconImageScaling),
            labelsStackView.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: Constants.defaultPadding),
            labelsStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            labelsStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
}
