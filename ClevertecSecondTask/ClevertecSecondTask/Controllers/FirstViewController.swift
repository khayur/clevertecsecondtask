//
//  ViewController.swift
//  ClevertecSecondTask
//
//  Created by Yury Khadatovich on 10.02.22.
//

import UIKit

class FirstViewController: BaseViewController {
    
    // MARK: - Private Properties
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(longPressGestureRecognized(longPress:)))
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(TableViewCell.self, forCellReuseIdentifier: "TableViewCell")
        tableView.rowHeight = Constants.defaultRowHeight
        tableView.addGestureRecognizer(longpress)
        tableView.backgroundColor = Constants.defaultBackgroundColor
        view.addSubview(tableView)
        return tableView
    }()
    
    private var model = Model.shared
    private var sourceIndexPath: IndexPath?
    private var snapshot: UIView?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activateConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - Private Methods
    
    private func activateConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
    
    private func customSnapshotFromView(inputView: UIView) -> UIView? {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0)
        if let CurrentContext = UIGraphicsGetCurrentContext() {
            inputView.layer.render(in: CurrentContext)
        }
        
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
            UIGraphicsEndImageContext()
            return nil
        }
        UIGraphicsEndImageContext()
        
        let snapshot = UIImageView(image: image)
        snapshot.layer.masksToBounds = false
        snapshot.layer.cornerRadius = 0
        snapshot.layer.shadowOffset = CGSize(width: -5, height: 0)
        snapshot.layer.shadowRadius = 5
        snapshot.layer.shadowOpacity = 0.4
        
        return snapshot
    }
    
    private func cleanupFromSnapshot() {
        self.sourceIndexPath = nil
        snapshot?.removeFromSuperview()
        self.snapshot = nil
    }
    
    // MARK: - Actions
    
    @objc private  func longPressGestureRecognized(longPress: UILongPressGestureRecognizer) {
        let state = longPress.state
        let location = longPress.location(in: self.tableView)
        
        guard let indexPath = self.tableView.indexPathForRow(at: location) else {
            cleanupFromSnapshot()
            return
        }
        
        switch state {
            case .began:
                guard let cell = self.tableView.cellForRow(at: indexPath) else { return }
                
                sourceIndexPath = indexPath
                snapshot = self.customSnapshotFromView(inputView: cell)
                guard  let snapshot = self.snapshot else { return }
                
                var center = cell.center
                snapshot.center = center
                snapshot.alpha = 0.0
                self.tableView.addSubview(snapshot)
                
                UIView.animate(withDuration: Constants.animationDuration, animations: {
                    center.y = location.y
                    snapshot.center = center
                    snapshot.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    snapshot.alpha = 1
                    cell.alpha = 0.0
                }, completion: { _ in
                    cell.isHidden = true
                })
                break
                
            case .changed:
                guard  let snapshot = self.snapshot,
                       let sourceIndexPath = self.sourceIndexPath else {
                           return
                       }
                
                var center = snapshot.center
                center.y = location.y
                snapshot.center = center
                
                if indexPath != sourceIndexPath {
                    model.rowsIndexes.swapAt(indexPath.row, sourceIndexPath.row)
                    self.tableView.moveRow(at: sourceIndexPath, to: indexPath)
                    self.sourceIndexPath = indexPath
                }
                break
                
            default:
                guard let cell = self.tableView.cellForRow(at: indexPath),
                      let snapshot = self.snapshot else {
                          return
                      }
                
                cell.alpha = 0
                
                UIView.animate(withDuration: Constants.animationDuration, animations: {
                    snapshot.center = cell.center
                    snapshot.transform = CGAffineTransform.identity
                    snapshot.alpha = 0
                    cell.alpha = 1
                }, completion: { _ in
                    cell.isHidden = false
                    self.cleanupFromSnapshot()
                    
                })
                break
        }
    }
    
}

//MARK: - UITableView DataSource

extension FirstViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.rowsIndexes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as? TableViewCell else { return UITableViewCell() }
        
        cell.configure(for: model.rowsIndexes[indexPath.row])
        return  cell
    }
    
}

//MARK: - UITableView Delegate

extension FirstViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            model.rowsIndexes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let choosedCell = tableView.cellForRow(at: indexPath) as? TableViewCell
        
        let secondViewController = SecondViewController()
        secondViewController.cellData = choosedCell?.getCellData()
        secondViewController.imageSize = choosedCell?.getIconImageSize()
        
        self.navigationController?.pushViewController(secondViewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}




