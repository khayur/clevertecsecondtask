//
//  TableViewCell.swift
//  ClevertecSecondTask
//
//  Created by Yury Khadatovich on 10.02.22.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    //MARK: - Public Properties
    private lazy var iconView: UIView = {
        let iconView =  UIView(frame: CGRect(origin: .zero, size: CGSize(width: iconViewSide, height: iconViewSide)))
        iconView.backgroundColor = Constants.iconBackgroungColor
        iconView.layer.cornerRadius = iconView.frame.height / 2
        iconView.addSubview(iconImageView)
        self.addSubview(iconView)
        return iconView
    }()
    
    private lazy var iconImageView: UIImageView = {
        UIImageView()
    }()
    
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = Constants.cellTitleLabelFont
        titleLabel.textColor = .darkGray
        
        self.addSubview(titleLabel)
        return titleLabel
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.font = Constants.cellDescriptionLabelFont
        descriptionLabel.textColor = .lightGray
        
        self.addSubview(descriptionLabel)
        return descriptionLabel
    }()
    
    private let iconViewSide: CGFloat = 50
    
    
    //MARK: - Public Methods
    
    override func prepareForReuse() {
        iconImageView.image = UIImage()
    }
    
    func configure(for row: Int) {
        let backgroundView = UIView()
        backgroundView.backgroundColor = Constants.cellBackgroundColor
        self.selectedBackgroundView = backgroundView
        
        configureIconImageView(for: row)
        configureLabels(for: row)
        activateConstraints()
    }
    
    func getCellData() -> (iconImage: UIImage?, titleLabelText: String?, descriptionLabelText: String?) {
        (iconImageView.image, titleLabel.text, descriptionLabel.text)
    }
    
    func getIconImageSize() -> CGSize {
        iconImageView.image?.size ?? CGSize(width: 0, height: 0)
    }
    
    //MARK: - Private Methods
    private func configureIconImageView(for row: Int) {
        let image: UIImage = UIImage(named: "\(row % 10)") ?? UIImage()
        iconImageView.image = image.withTintColor(.black)
    }
    
    private func configureLabels(for row: Int) {
        titleLabel.text = Constants.cellTitleLabelText + String(row)
        
        descriptionLabel.text = Constants.cellDescriptionLabelText + String(row)
    }
    
    private func activateConstraints() {
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            self.bottomAnchor.constraint(equalTo: iconView.bottomAnchor, constant: Constants.defaultPadding),
            self.topAnchor.constraint(equalTo: iconView.topAnchor, constant: -Constants.defaultPadding),
            self.leadingAnchor.constraint(equalTo: iconView.leadingAnchor, constant: -Constants.defaultPadding),
            iconView.widthAnchor.constraint(equalToConstant: iconViewSide),
            iconImageView.centerXAnchor.constraint(equalTo: iconView.centerXAnchor),
            iconImageView.centerYAnchor.constraint(equalTo: iconView.centerYAnchor),
            iconImageView.widthAnchor.constraint(equalToConstant: iconViewSide / Constants.cellIconImageScaling),
            iconImageView.heightAnchor.constraint(equalToConstant: iconViewSide / Constants.cellIconImageScaling),
            titleLabel.topAnchor.constraint(equalTo: iconView.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: Constants.defaultPadding),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            descriptionLabel.bottomAnchor.constraint(equalTo: iconView.bottomAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
    }
                
}
